import { EDGE_MAX_VALUE, PACKING_LOGIC, IOption, EDGE_MIN_VALUE } from "./maxrects-packer";
import { Rectangle, IRectangle } from "./geom/Rectangle";
import { Bin } from "./abstract-bin";

export class MaxRectsBin<T extends IRectangle = Rectangle> extends Bin<T> {
    public width: number;
    public height: number;
    public minWidth: number;
    public minHeight: number;
    public freeRects: Rectangle[] = [];
    public rects: T[] = [];
    public fitFuncs = {};
    private verticalExpand: boolean = false;
    private stage: Rectangle;
    private border: number;
    public options: IOption = {
        smart: true,
        pot: true,
        square: true,
        allowRotation: false,
        tag: false,
        exclusiveTag: true,
        border: 0,
        logic: PACKING_LOGIC.LONG_SIDE_FIT
    };

    constructor (
        public maxWidth: number = EDGE_MAX_VALUE,
        public maxHeight: number = EDGE_MAX_VALUE,
        public padding: number = 0,
        options: IOption = {},
        minWidth: number = EDGE_MIN_VALUE,
        minHeight: number = EDGE_MIN_VALUE,
    ) {
        super();
        this.options = { ...this.options, ...options };
        this.width = this.options.smart ? 0 : maxWidth;
        this.height = this.options.smart ? 0 : maxHeight;
        this.minHeight = minHeight;
        this.minWidth = minWidth;
        this.border = this.options.border ? this.options.border : 0;
        this.pushFreeRect(new Rectangle(
            this.maxWidth + this.padding - this.border * 2,
            this.maxHeight + this.padding - this.border * 2,
            this.border,
            this.border));
        this.stage = new Rectangle(this.width, this.height);
        this.initFitFunc()
    }

    public add (rect: T): T | undefined;
    public add (width: number, height: number, data: any): T | undefined;
    public add (...args: any[]): any {
        let data: any;
        let rect: IRectangle;
        if (args.length === 1) {
            if (typeof args[0] !== 'object') throw new Error("MacrectsBin.add(): Wrong parameters");
            rect = args[0] as T;
            // Check if rect.tag match bin.tag, if bin.tag not defined, it will accept any rect
            let tag = (rect.data && rect.data.tag) ? rect.data.tag : rect.tag ? rect.tag : undefined;
            if (this.options.tag && this.options.exclusiveTag && this.tag !== tag) return undefined;
        } else {
            data = args.length > 2 ? args[2] : null;
            // Check if data.tag match bin.tag, if bin.tag not defined, it will accept any rect
            if (this.options.tag && this.options.exclusiveTag) {
                if (data && this.tag !== data.tag) return undefined;
                if (!data && this.tag) return undefined;
            }
            rect = new Rectangle(args[0], args[1]);
            rect.data = data;
            rect.setDirty(false);
        }

        const result = this.place(rect);
        if (result) this.rects.push(result);
        return result;
    }

    public repack (): T[] | undefined {
        let unpacked: T[] = [];
        this.reset();
        // re-sort rects from big to small
        this.rects.sort((a, b) => {
            const result = Math.max(b.width, b.height) - Math.max(a.width, a.height);
            if (result === 0 && a.hash && b.hash) {
                return a.hash > b.hash ? -1 : 1;
            } else return result;
        });
        for (let rect of this.rects) {
            if (!this.place(rect)) {
                unpacked.push(rect);
            }
        }
        for (let rect of unpacked) this.rects.splice(this.rects.indexOf(rect), 1);
        return unpacked.length > 0 ? unpacked : undefined;
    }

    public reset (deepReset: boolean = false, resetOption: boolean = false): void {
        if (deepReset) {
            if (this.data) delete this.data;
            if (this.tag) delete this.tag;
            this.rects = [];
            if (resetOption) {
                this.options = {
                    smart: true,
                    pot: true,
                    square: true,
                    allowRotation: false,
                    tag: false,
                    border: 0
                };
            }
        }
        this.width = this.options.smart ? 0 : this.maxWidth;
        this.height = this.options.smart ? 0 : this.maxHeight;
        this.border = this.options.border ? this.options.border : 0;
        this.freeRects = [new Rectangle(
            this.maxWidth + this.padding - this.border * 2,
            this.maxHeight + this.padding - this.border * 2,
            this.border,
            this.border)];
        this.stage = new Rectangle(this.width, this.height);
        this._dirty = 0;
    }

    public clone (): MaxRectsBin<T> {
        let clonedBin: MaxRectsBin<T> = new MaxRectsBin<T>(this.maxWidth, this.maxHeight, this.padding, this.options);
        for (let rect of this.rects) {
            clonedBin.add(rect);
        }
        return clonedBin;
    }

    private place (rect: IRectangle): T | undefined {
        // recheck if tag matched
        let tag = (rect.data && rect.data.tag) ? rect.data.tag : rect.tag ? rect.tag : undefined;
        if (this.options.tag && this.options.exclusiveTag && this.tag !== tag) return undefined;

        let node: IRectangle | undefined;
        let allowRotation: boolean | undefined;
        // getter/setter do not support hasOwnProperty()
        if (rect.hasOwnProperty("_allowRotation") && rect.allowRotation !== undefined) {
            allowRotation = rect.allowRotation; // Per Rectangle allowRotation override packer settings
        } else {
            allowRotation = this.options.allowRotation;
        }
        node = this.findNode(rect.width + this.padding, rect.height + this.padding, allowRotation);

        if (node) {
            this.updateBinSize(node);
            let numRectToProcess = this.freeRects.length;
            let i: number = 0;
            while (i < numRectToProcess) {
                if (this.splitNode(this.freeRects[i], node)) {
                    this.freeRects.splice(i, 1);
                    numRectToProcess--;
                    i--;
                }
                i++;
            }
            this.pruneFreeList();
            this.verticalExpand = this.width > this.height ? true : false;
            rect.x = node.x;
            rect.y = node.y;
            if (rect.rot === undefined) rect.rot = false;
            rect.rot = node.rot ? !rect.rot : rect.rot;
            this._dirty ++;
            return rect as T;
        } else if (!this.verticalExpand) {
            if (this.updateBinSize(new Rectangle(
                rect.width + this.padding, rect.height + this.padding,
                this.width + this.padding - this.border, this.border
            )) || this.updateBinSize(new Rectangle(
                rect.width + this.padding, rect.height + this.padding,
                this.border, this.height + this.padding - this.border
            ))) {
                return this.place(rect);
            }
        } else {
            if (this.updateBinSize(new Rectangle(
                rect.width + this.padding, rect.height + this.padding,
                this.border, this.height + this.padding - this.border
            )) || this.updateBinSize(new Rectangle(
                rect.width + this.padding, rect.height + this.padding,
                this.width + this.padding - this.border, this.border
            ))) {
                return this.place(rect);
            }
        }
        return undefined;
    }

    private findNode (width: number, height: number, allowRotation?: boolean): Rectangle | undefined {
        let score: number = Number.MAX_VALUE;
        let areaFit: number;
        let r: Rectangle;
        let bestNode: Rectangle | undefined;
        for (let i in this.freeRects) {
            r = this.freeRects[i];
            if (r.width >= width && r.height >= height) {
                areaFit = this.getFitScore(r, width, height, this.options.logic,false)
                if (areaFit < score) {
                    bestNode = new Rectangle(width, height, r.x, r.y);
                    score = areaFit;
                }
            }

            if (!allowRotation) continue;
    
            if (r.width >= height && r.height >= width) {
                areaFit = this.getFitScore(r, height, width, this.options.logic, true)
                if (areaFit < score) {
                    bestNode = new Rectangle(height, width, r.x, r.y, true); // Rotated node
                    score = areaFit;
                }
            }
        }
        return bestNode;
    }


    private initFitFunc(){
        this.fitFuncs = {
            [PACKING_LOGIC.MAX_AREA_FIT]: this.getAreaScore.bind(this),    
            [PACKING_LOGIC.LONG_SIDE_FIT]: this.getLongSideScore.bind(this),  
            [PACKING_LOGIC.SHORT_SIDE_FIT]: this.getShortSideScore.bind(this),
            [PACKING_LOGIC.BOTTOM_LEFT_FIT]: this.getBottomLeftScore.bind(this),
            [PACKING_LOGIC.CONTACT_POINT_RULE_FIT]: this.getContactPointScore.bind(this),
        }
    }

    //优先匹配左下角
    private getBottomLeftScore (rect: IRectangle, width: number, height: number,isRotation :boolean = false): number {
       return  rect.y + (isRotation ? width : height);
    }

    private CommonIntervalLength(i1start, i1end,  i2start, i2end) {
        if (i1end < i2start || i2end < i1start)
            return 0;
        return Math.min(i1end, i2end) - Math.max(i1start, i2start);
    }

    //优先
    private getContactPointScore (rect: IRectangle, width: number, height: number,isRotation :boolean = false): number {
        let score = 0 
        if(rect.x == 0 || (rect.x + width) == this.width){
            score -= height 
        }
        if(rect.y == 0 || (rect.y + height) == this.height){
            score -= width 
        }
        for( let userect of this.rects){
            if (userect.x == rect.x + width || userect.x + userect.width == rect.x){ //上下贴边的情况下,宽度刚好相等,或者与已使用的矩形左边相贴
                score -= this.CommonIntervalLength(userect.y, userect.y + userect.height, rect.y, rect.y + height);//相邻值更高
            }
            if (userect.y == rect.y + height || userect.y + userect.height == rect.y){
                score -= this.CommonIntervalLength(userect.x, userect.x + userect.width, rect.x, rect.x + width);
            }
        }
        return score
    }

    //面积
    private getAreaScore (rect: IRectangle, width: number, height: number,isRotation :boolean = false): number {
        return  rect.width * rect.height - width * height
    }

    //短边差值
    private getShortSideScore(rect: IRectangle, width: number, height: number,isRotation:boolean = false){
        return Math.min(rect.width, rect.height) - Math.min(width, height);
    }

    //长边差值
    private getLongSideScore(rect: IRectangle, width: number, height: number,isRotation:boolean = false){
        return Math.max(rect.width, rect.height) - Math.max(width, height);
    }

    //权重 计算
    private getFitScore(rect : Rectangle , width:number , height : number , logic: PACKING_LOGIC = PACKING_LOGIC.LONG_SIDE_FIT , isRotation: boolean = false) 
    {
        let func = this.fitFuncs[logic]
        if(func){
            return func(rect , width , height , isRotation)
        }
        return 0        
    }
    
    //插入空闲矩阵
    private pushFreeRect (rect: Rectangle): void {
        //过滤小于最小宽度和高度的
        if (rect.width < this.minWidth || rect.height < this.minHeight) return;
        this.freeRects.push(rect);
    }

    //拆分出新的可用rect
    private splitNode (freeRect: IRectangle, usedNode: IRectangle): boolean {     
        if (!freeRect.collide(usedNode)) return false;

        //竖向拆分
        if (usedNode.x < freeRect.x + freeRect.width && usedNode.x + usedNode.width > freeRect.x) {
            //顶部
            if (usedNode.y > freeRect.y && usedNode.y < freeRect.y + freeRect.height) {
                let newNode: Rectangle = new Rectangle(freeRect.width, usedNode.y - freeRect.y, freeRect.x, freeRect.y);
                this.pushFreeRect(newNode);
            }
            //底部
            if (usedNode.y + usedNode.height < freeRect.y + freeRect.height) {
                let newNode = new Rectangle(
                    freeRect.width,
                    freeRect.y + freeRect.height - (usedNode.y + usedNode.height),
                    freeRect.x,
                    usedNode.y + usedNode.height
                );
                this.pushFreeRect(newNode);
            }
        }

        //横向拆分
        if (usedNode.y < freeRect.y + freeRect.height &&
            usedNode.y + usedNode.height > freeRect.y) {
            //左边
            if (usedNode.x > freeRect.x && usedNode.x < freeRect.x + freeRect.width) {
                let newNode = new Rectangle(usedNode.x - freeRect.x, freeRect.height, freeRect.x, freeRect.y);
                this.pushFreeRect(newNode);
            }
            //右边
            if (usedNode.x + usedNode.width < freeRect.x + freeRect.width) {
                let newNode = new Rectangle(
                    freeRect.x + freeRect.width - (usedNode.x + usedNode.width),
                    freeRect.height,
                    usedNode.x + usedNode.width,
                    freeRect.y
                );
                this.pushFreeRect(newNode);
            }
        }
        return true;
    }

    private pruneFreeList () {
        //遍历删除 多余rect
        let i: number = 0;
        let j: number = 0;
        let len: number = this.freeRects.length;
        while (i < len) {
            j = i + 1;
            let tmpRect1 = this.freeRects[i];
            while (j < len) {
                let tmpRect2 = this.freeRects[j];
                if (tmpRect2.contain(tmpRect1)) {
                    this.freeRects.splice(i, 1);
                    i--;
                    len--;
                    break;
                }
                if (tmpRect1.contain(tmpRect2)) {
                    this.freeRects.splice(j, 1);
                    j--;
                    len--;
                }
                j++;
            }
            i++;
        }
    }

    private updateBinSize (node: IRectangle): boolean {
        if (!this.options.smart) return false;
        if (this.stage.contain(node)) return false;
        let tmpWidth: number = Math.max(this.width, node.x + node.width - this.padding + this.border);
        let tmpHeight: number = Math.max(this.height, node.y + node.height - this.padding + this.border);
        if (this.options.allowRotation) {
            // 旋转 获取 更优解
            const rotWidth: number = Math.max(this.width, node.x + node.height - this.padding + this.border);
            const rotHeight: number = Math.max(this.height, node.y + node.width - this.padding + this.border);
            if (rotWidth * rotHeight < tmpWidth * tmpHeight) {
                tmpWidth = rotWidth;
                tmpHeight = rotHeight;
            }
        }
        if (this.options.pot) {
            tmpWidth = Math.pow(2, Math.ceil(Math.log(tmpWidth) * Math.LOG2E));
            tmpHeight = Math.pow(2, Math.ceil(Math.log(tmpHeight) * Math.LOG2E));
        }
        if (this.options.square) {
            tmpWidth = tmpHeight = Math.max(tmpWidth, tmpHeight);
        }
        if (tmpWidth > this.maxWidth + this.padding || tmpHeight > this.maxHeight + this.padding) {
            return false;
        }
        this.expandFreeRects(tmpWidth + this.padding, tmpHeight + this.padding);
        this.width = this.stage.width = tmpWidth;
        this.height = this.stage.height = tmpHeight;
        return true;
    }

    private expandFreeRects (width: number, height: number) {
        this.freeRects.forEach((freeRect, index) => {
            if (freeRect.x + freeRect.width >= Math.min(this.width + this.padding - this.border, width)) {
                freeRect.width = width - freeRect.x - this.border;
            }
            if (freeRect.y + freeRect.height >= Math.min(this.height + this.padding - this.border, height)) {
                freeRect.height = height - freeRect.y - this.border;
            }
        }, this);
        this.pushFreeRect(new Rectangle(
            width - this.width - this.padding,
            height - this.border * 2,
            this.width + this.padding - this.border,
            this.border));
        this.pushFreeRect(new Rectangle(
            width - this.border * 2,
            height - this.height - this.padding,
            this.border,
            this.height + this.padding - this.border));
        this.freeRects = this.freeRects.filter(freeRect => {
            return !(freeRect.width <= 0 || freeRect.height <= 0 || freeRect.x < this.border || freeRect.y < this.border);
        });
        this.pruneFreeList();
    }
}
